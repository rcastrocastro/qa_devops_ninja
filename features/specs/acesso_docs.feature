#language: pt

Funcionalidade: Acessando a feature Docs
    Eu como usuário do website
    Desejo ter acesso a área de docs 
    Para que possa visualizar os tutoriais 

Cenário: Acesso ao Tutorial
    Dado que o usuário realiza o acesso ao website
    Quando acessar a área de docs
    Então deverá visializar o tutorial de instalação "Docker"