Dado("que o usuário realiza o acesso ao website") do
    @page.call(Home).go
end
  
Quando("acessar a área de docs") do
    @page.call(Home).access_tutorial
end
  
Então("deverá visializar o tutorial de instalação {string}") do |expect_docker|
    expect(page).to have_content expect_docker
end