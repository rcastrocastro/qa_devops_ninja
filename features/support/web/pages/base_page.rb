# frozen_string_literal: true

# Class BasePage
class BasePage
  include Capybara::DSL
end
