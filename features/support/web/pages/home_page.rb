require_relative './base_page.rb'

class Home < BasePage
    attr_reader :btn_docs


    def initialize
        @btn_docs = EL["btn_docs"]
    end

    def go
        visit("/")
    end

    def access_tutorial
        find(btn_docs).click
    end
end